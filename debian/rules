#!/usr/bin/make -f

##############
# Legal stuff
##############

# Copyright (c) 2003-2006, 2011-2014 Ludovic Brenta <lbrenta@debian.org>
# Copyright (c) 2010-2020 Nicolas Boulenguez <nicolas@debian.org>

# This build script is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# On Debian systems, the full text of the GPL is in the file
# /usr/share/common-licenses/GPL-3.

.SUFFIXES:

########################
# Guess some variables.
########################

DEB_BUILD_MAINT_OPTIONS := hardening=+all
include /usr/share/dpkg/architecture.mk
ifneq (,$(filter $(DEB_HOST_ARCH), ppc64el))
  DEB_CFLAGS_MAINT_APPEND = -O2
  DEB_CFLAGS_MAINT_STRIP = -O3
  # This also affects ADAFLAGS, see debian_packaging-*.mk below.
endif
DEB_LDFLAGS_MAINT_APPEND := \
  -Wl,--no-allow-shlib-undefined \
  -Wl,--no-copy-dt-needed-entries \
  -Wl,--no-undefined
DPKG_EXPORT_BUILDFLAGS := 1
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/pkg-info.mk

PKG_CONFIG_LIBRARIES := gtk+-3.0 glib-2.0 pygobject-3.0 python-2.7

ifneq (,$(wildcard /usr/bin/gnatgcc))
  # Avoid this part during -indep builds.

  gnat_version := $(shell gnatgcc -dumpversion)
  include /usr/share/ada/debian_packaging-$(gnat_version).mk

  CLANG_VERSION := $(shell dpkg -s libclang-dev \
    | sed -nr '/^Depends: libclang-(.*)-dev.*/{s//\1/p;q}')
endif

######################################################################
%:
	dh $@

# Do not try to call an upstream configure or Makefile.
.PHONY: $(addprefix override_dh_auto_, \
  clean configure build-arch build-indep test install)

######################################################################

# Note: the script generates both Ada and Python sources, the latter
# are embedded into the users_guide.
override_dh_auto_build-arch: docs/users_guide/GPS/generated_hooks.py
docs/users_guide/_build: docs/users_guide/GPS/generated_hooks.py
docs/users_guide/GPS/generated_hooks.py: kernel/src/hooks.py
	rm -fr kernel/generated
	mkdir kernel/generated
	cd kernel/src && python3 hooks.py
override_dh_auto_clean::
	rm -fr kernel/generated
	rm -f docs/users_guide/GPS/generated_hooks.py

override_dh_auto_build-arch: libclang/gen
libclang/gen:
	$(MAKE) -C libclang \
	  CLANG_SRC_DIR=/usr/lib/llvm-$(CLANG_VERSION) \
	  GCC_DUMPMACHINE=$(DEB_HOST_MULTIARCH)
override_dh_auto_clean::
	$(MAKE) -C libclang clean

######################################################################
override_dh_auto_build-arch: | obj
	gprbuild $(BUILDER_OPTIONS) debian/build_gps.gpr \
	  -XDEB_HOST_GNU_TYPE=$(DEB_HOST_GNU_TYPE) \
	  -XLIBS=/usr/lib/llvm-$(CLANG_VERSION)/lib/libclang.so \
	  "-XPKG_CONFIG_CFLAGS=`pkg-config --cflags $(PKG_CONFIG_LIBRARIES)`" \
	  "-XPKG_CONFIG_LIBS=`pkg-config --libs $(PKG_CONFIG_LIBRARIES)`"
obj:
	mkdir $@
override_dh_auto_clean::
	rm -fr obj

database := kernel/generated/gps-kernel-properties-database.ads
database_options := \
  -dbtype=sqlite \
  -api=GPS.Kernel.Properties.Database \
  -adacreate \
  -dbmodel=../src/properties_schema.txt
override_dh_auto_build-arch: $(database)
$(database):
	cd $(dir $@) && gnatcoll_db2ada $(database_options)
override_dh_auto_clean::
	rm -f $(database)

######################################################################
configured := \
  common/core/src/config.ads \
  docs/gnatstudio-welcome.html \
  docs/gnatstudio-welcome-public.html \
  share/support/core/protocols.py
override_dh_auto_configure: $(configured)
$(configured): %: %.in
	sed \
	    -e 's/@CURRENT_YEAR@/20$(DEB_VERSION_UPSTREAM)/' \
	    -e 's/@GPS_VERSION@/$(DEB_VERSION_UPSTREAM)/' \
	    -e 's/@TARGET@/$(DEB_HOST_GNU_TYPE))/' \
	    -e 's/@TARGET_IS_DARWIN@/False/' \
	    -e 's!@prefix@!/usr!' \
	    -e 's/@HOST_TYPE@/Unix/' \
	    -e 's/@STRIP_CR@/False/' \
	    -e 's/@CAN_OUTPUT@/True/' \
	    -e 's!@EXEC_COMMAND@!/bin/sh -c!' \
	    -e 's/@DEFAULT_PS@/ps/' \
	    -e 's/@PRINT_CMD@/lpr/' \
	    -e 's/@OPAQUE_MDI@/True/' \
	    -e 's/@DIFF_CMD@/diff -u/' \
	    -e 's/@DIFF3_CMD@/diff3/' \
	    -e 's/@PATCH_CMD@/patch/' \
	    -e 's/@EXECUTION_WINDOW@/True/' \
	    -e 's/@CHARSET@/UTF-8/' \
	    -e 's/@SSH_ESCAPE@/~/' \
	    $< > $@
override_dh_auto_clean::
	rm -f $(configured)

override_dh_auto_configure: VERSION.txt
VERSION.txt:
	echo '$(DEB_VERSION_UPSTREAM)' > $@
override_dh_auto_clean::
	rm -f VERSION.txt

######################################################################
DOCS := \
 docs/users_guide/_build \
 docs/tutorial/_build \
 docs/programmers_guide/_build \
 docs/release_notes/build \
 gnatdoc/docs/users_guide/_build

ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
override_dh_auto_build-indep: $(DOCS)
  # Sphinx creates Python precompiled files in every imported module,
  # we do not want them installed.
	find . -name "*.pyc" -delete
	find . -type d -name "__pycache__" -print0 | xargs -r0 rmdir
endif

# Note: see docs/users_guide/GPS/generated_hooks.py above.
$(DOCS):
	$(MAKE) -C $(dir $@) html latexpdf

override_dh_auto_clean::
	rm -fr $(DOCS)
	find . -name "*.pyc" -delete
	find . -type d -name "__pycache__" -print0 | xargs -r0 rmdir

######################################################################
datadir := debian/gnatstudio-common/usr/share/gnatstudio

.PHONY: execute_after_dh_install-indep
execute_after_dh_install-indep:
  # Documentation and license are not useful here.
  # libadalang is disabled because it is not packaged in Debian
  # No -f, warn me if the list becomes obsolete.
	rm $(datadir)/color_themes/themes/*/README.* \
	   $(datadir)/color_themes/themes/*/LICENSE*
	rm -fr $(datadir)/support/core/lal_utils
